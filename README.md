# Lumberjack

## Description

Lumberjack is a browser addon that allows you to save your logins.

In order to save one, enter it in the loggin field of the desired website and then simply press `Ctrl+Shift+L` (or `Cmd+Shift+L` on Mac OS X). The loggin will be reminded and synchronized with your Firefox account. To forget it repeat the operation while leaving the loggin field blank.

## Limitations

 - The loggin field have to be an input element and have an ID.

## Download

- Firefox version available [here](https://addons.mozilla.org/fr/firefox/addon/lumberjack/).

## General info

- Author: Clément Saccoccio
- Licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)