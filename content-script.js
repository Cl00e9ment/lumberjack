// Copyright 2019, Clément Saccoccio

/*
This file is part of Lumberjack.

Lumberjack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Lumberjack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lumberjack. If not, see <https://www.gnu.org/licenses/>.
*/

(() => {

	'use strict';

	const url = window.location.origin + window.location.pathname;
	loadStoredLoggin();
	listenForLogginToStore();

	function sleep(millis) {
		return new Promise(resolve => setTimeout(resolve, millis));
	}

	function focus(element) {
		const initialTabIndex = element.getAttribute('tabindex');
		element.setAttribute('tabindex', '-1');
		element.focus();
		element.setAttribute('tabindex', initialTabIndex);
	}

	function listenForLogginToStore() {

		browser.runtime.connect().onMessage.addListener(msg => {

			if (msg !== 'save_loggin') return;

			let logginField = document.activeElement;
			while (logginField.nodeName.toLowerCase() === 'frame') {
				logginField = (logginField.contentDocument || logginField.contentWindow.document).activeElement;
			}
			
			const id = logginField.id;
			if (id === '') return;
			const loggin = logginField.value;
			if (loggin === undefined) return;
	
			if (loggin === '') {
				browser.storage.sync.remove(url);
			} else {
				const data = {};
				data[url] = {
					id: id,
					loggin: loggin
				};
				browser.storage.sync.set(data);

				const passwordField = getPasswordField();
				if (passwordField !== null) focus(passwordField);
			}
		});
	}

	async function loadStoredLoggin() {
		
		let data = await browser.storage.sync.get(url);
		if (data[url] === undefined) return;
		data = data[url];

		let logginField;
		while ((logginField = getLogginField(data.id)) === null) {
			await sleep(100);
		}
		logginField.value = data.loggin;

		const passwordField = getPasswordField();
		if (passwordField !== null) {
			focus(passwordField);
			const tryUntil = Date.now() + 1000;
			while (passwordField !== document.activeElement && Date.now() < tryUntil) {
				await sleep(100);
				focus(passwordField);
			}
		}
	}

	function getLogginField(id) {
		return getLogginField1(id) || getLogginField2();
	}

	function getLogginField1(id, doc) {
		if (doc === undefined) doc = document;
		let logginField = doc.getElementById(id);
		if (logginField !== null) return logginField;
		for (const frame of doc.getElementsByTagName('frame')) {
			logginField = getLogginField1(id, frame.contentDocument || frame.contentWindow.document);
			if (logginField !== null) return logginField;
		}
		return null;
	}

	function getLogginField2(doc) {
		if (doc === undefined) doc = document;
		const activeElement = doc.activeElement;
		if (activeElement !== null
			&& activeElement.tagName !== undefined
			&& activeElement.tagName.toLowerCase() === 'input'
			&& activeElement.type !== undefined
			&& activeElement.type.toLowerCase() === 'text') {
			return activeElement;
		}
		for (const frame of doc.getElementsByTagName('frame')) {
			const logginField = getLogginField2(frame.contentDocument || frame.contentWindow.document);
			if (logginField !== null) return logginField;
		}
		return null;
	}

	function getPasswordField(doc) {
		if (doc === undefined) doc = document;
		let passwordField = doc.querySelector('input[type="password"]');
		if (passwordField !== null) return passwordField;
		for (const frame of doc.getElementsByTagName('frame')) {
			passwordField = getPasswordField(frame.contentDocument || frame.contentWindow.document);
			if (passwordField !== null) return passwordField;
		}
		return null;
	}

})();
