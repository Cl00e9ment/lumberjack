// Copyright 2019, Clément Saccoccio

/*
This file is part of Lumberjack.

Lumberjack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Lumberjack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lumberjack. If not, see <https://www.gnu.org/licenses/>.
*/

browser.runtime.onConnect.addListener(port => {
	browser.commands.onCommand.addListener(port.postMessage);
});